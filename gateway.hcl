job "gateway-ci" {

  datacenters = ["[[.dc]]"]

  type = "service"

  group "gateway-service" {
    count = [[.instances]]
    task "api" {
      driver = "docker"
      config {
        image = "kmandalas/gateway-service:[[.version]]"
        network_mode = "host"
        port_map {
          http = 8000
        }
      }
      resources {
        cpu    = [[.cpu]]
        memory = [[.memory]]
        network {
          port "http" {}
        }
      }
      service {
        name = "gateway-service"
        port = "http"
        tags = ["ci", "[[.version]]"]
      }
    }
    restart {
      attempts = 1
    }
  }

}
